#! /usr/bin/env python3.3
#
# -*- mode: Python-*-
#
import io
import re
import os.path
import sys
from time import localtime, strftime
from collections import namedtuple
import unittest

Success = namedtuple('Success', ('name',))
Failure = namedtuple('Failure', (
    'name',
    'actual',
    'expected',
    'msg'))
Error = namedtuple('Error', ('name', 'msg'))

PATTERN = '(?:\[A\](?P<actual>.*)\[A\].*\[E\](?P<expected>.*)\[E\])?(?P<msg>.*)'

def match(msg):
    """Matches actual and expected results from nose xml output in failure
    elements' message attribute.

    Arguments:
    msg     The contents of the message attribute of the failure element.

    Returns:
    A dictionary with
    actual      The actual value.
    adiff       A string that points to the differance in the actual value.
    expected    The expected value.
    ediff       A string that points to the differance in the expected value.

    Example:
    actual:     Hello world?
    adiff:                 ^
    expected:   Hello, world!
    ediff            +      ^
    """

    res = re.search(pattern=PATTERN, string=msg, flags=re.MULTILINE)

    if res:
        return res.groupdict()
    else:
        return msg

class MyTestResult(unittest.TestResult):
    def __init__(self):
        super().__init__()
        self.failfast = False
        self.tests=[]
        
    def addError(self, test, err):
        super().addError(test, err)
        self.tests.append(Error(test._testMethodName, err[1].args[0]))


    def addFailure(self, test, err):
        super().addFailure(test, err)

        r = match(err[1].args[0])
        self.tests.append(Failure(test._testMethodName, r['actual'],r['expected'],r['msg']))

    def addSuccess(self, test):
        self.tests.append(Success(test._testMethodName))

def run_tests(testfile):
    """Run tests in the test defined by testspec and return 
    MyTestResult instance with the results, or None if no tests could be found."""
    result = MyTestResult()
    test_loader = unittest.defaultTestLoader

    if os.path.exists(testfile):
        test_suite = test_loader.discover(*os.path.split(testfile))
    else:
        test_suite = test_loader.loadTestsFromName(testfile)

    if test_suite.countTestCases()  > 0:
        test_suite.run(result)
        return result
    else:
        print("No test cases found")
        return None

def paint(text, col):
    """ASCII code painter"""

    return '\033[{col}m{text}\033[0m'.format(col=col, text=text,)

def nopaint(text, col):
    """No-op painter"""
    return text


def print_results(results, paint=paint, output_file=sys.stdout):
    GREEN = 32
    RED = 31
    MAGENTA = 35
    BLUE = 34
    YELLOW = 33

    print(file=output_file)
    time = strftime("%Y-%m-%d %H:%M:%S", localtime())

    print(paint('[INFO] Test time: {}'.format(time), BLUE), file=output_file)

    for test in results.tests:
        print(file=output_file)       
        if isinstance(test, Success):
            print(paint('[PASS] {}'.format(test.name), GREEN), file=output_file)
        
        elif isinstance(test, Failure):
            print(paint('[FAIL] {}'.format(test.name), RED), file=output_file)
            print(paint('       {}'.format('-' * len(test.name)), RED), file=output_file)
            print(file=output_file)
            if test.actual:
                print(paint('       {}'.format(test.actual), RED), file=output_file)
            if test.expected:
                print(paint('       {}'.format(test.expected), RED), file=output_file)
            print(file=output_file)
            print(paint('       [?] {}'.format(test.msg), RED), file=output_file)
        
        elif isinstance(test, Error):
            print('     ! Error  {name}:\n {error}'.format(name=test.name, error=test.msg), file=output_file)
        
    print(file=output_file)
    print(paint('[INFO] {}/{} tests succeeded'.format(len([t for t in results.tests if isinstance(t,Success)]), len(results.tests)), BLUE), file=output_file)
    print(file=output_file)



